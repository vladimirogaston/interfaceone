public interface Saludator {
    String saludar(String name);
    String saludar();
}