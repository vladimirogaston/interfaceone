import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SaludatorTest {

    private static Saludator saludador = new Saludator() {
        @Override
        public String saludar(String name) {
            return "Hola " + name;
        }

        @Override
        public String saludar() {
            return "Holis!";
        }
    };

    @Test
    void saludar() {
        assertNotNull(saludador.saludar("John Doe"));
    }

    @Test
    void testSaludar() {
        assertEquals("Holis!", saludador.saludar());
    }
}